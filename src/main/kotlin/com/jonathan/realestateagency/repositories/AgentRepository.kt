package com.jonathan.realestateagency.repositories

import com.jonathan.realestateagency.entities.Agent
import org.springframework.data.jpa.repository.JpaRepository

interface AgentRepository: JpaRepository<Agent, Long> {
}