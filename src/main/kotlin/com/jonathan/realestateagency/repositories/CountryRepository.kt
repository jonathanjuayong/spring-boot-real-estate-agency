package com.jonathan.realestateagency.repositories

import com.jonathan.realestateagency.entities.Country
import org.springframework.data.jpa.repository.JpaRepository

interface CountryRepository: JpaRepository<Country, Long> {
}