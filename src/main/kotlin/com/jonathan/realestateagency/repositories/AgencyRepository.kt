package com.jonathan.realestateagency.repositories

import com.jonathan.realestateagency.entities.Agency
import org.springframework.data.jpa.repository.JpaRepository

interface AgencyRepository: JpaRepository<Agency, Long> {
}