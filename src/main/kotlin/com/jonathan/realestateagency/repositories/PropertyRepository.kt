package com.jonathan.realestateagency.repositories

import com.jonathan.realestateagency.entities.Property
import org.springframework.data.jpa.repository.JpaRepository

interface PropertyRepository: JpaRepository<Property, Long> {
}