package com.jonathan.realestateagency.repositories

import com.jonathan.realestateagency.entities.Address
import org.springframework.data.jpa.repository.JpaRepository

interface AddressRepository: JpaRepository<Address, Long> {
}