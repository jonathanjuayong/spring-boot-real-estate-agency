package com.jonathan.realestateagency.services

import com.jonathan.realestateagency.entities.Agent

interface AgentService {
    fun getAllAgents(): List<Agent>
    fun getAgentById(id: Long): Agent
    fun createAgent(agent: Agent): Agent
    fun updateAgent(id: Long, agent: Agent): Agent
    fun deleteAgent(id: Long)
}