package com.jonathan.realestateagency.services

import com.jonathan.realestateagency.entities.Agency

interface AgencyService {
    fun getAllAgencies(): List<Agency>
    fun getAgencyById(id: Long): Agency
    fun createAgency(agency: Agency): Agency
    fun updateAgency(id: Long, agency: Agency): Agency
    fun deleteAgency(id: Long)
}