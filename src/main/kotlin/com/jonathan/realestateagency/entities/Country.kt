package com.jonathan.realestateagency.entities

import javax.persistence.*

@Entity
@Table(name = "countries")
data class Country(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @Column(
        nullable = false,
        updatable = true,
        name = "name"
    )
    val name: String = ""
)