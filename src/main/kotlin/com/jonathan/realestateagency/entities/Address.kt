package com.jonathan.realestateagency.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
@Table(name = "addresses")
data class Address(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,

    @Column(
        nullable = false,
        updatable = true,
        name = "street"
    )
    val street: String = "",

    @Column(
        nullable = false,
        updatable = true,
        name = "city"
    )
    val city: String = "",

    @Column(
        nullable = false,
        updatable = true,
        name = "state"
    )
    val state: String = "",

    @JsonIgnoreProperties(value = ["id"], allowSetters = true)
    @OneToOne
    @JoinColumn(name = "country_id", referencedColumnName = "id")
    val country: Country
)