package com.jonathan.realestateagency.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
@Table(name = "agents")
data class Agent(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @Column(
        nullable = false,
        updatable = true,
        name = "first_name"
    )
    var firstName: String = "",

    @Column(
        nullable = false,
        updatable = true,
        name = "last_name"
    )
    var lastName: String = "",

    @JsonIgnoreProperties(value = ["agents"], allowSetters = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agency_id", referencedColumnName = "id")
    var agency: Agency,

    @JsonIgnoreProperties(value = ["agent"], allowSetters = true)
    @OneToMany(mappedBy = "agent", fetch = FetchType.LAZY)
    var properties: MutableList<Property> = mutableListOf()
)