package com.jonathan.realestateagency.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
@Table(name = "properties")
data class Property(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,

    @Column(
        nullable = false,
        updatable = true,
        name = "name"
    )
    var name: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "price"
    )
    var price: Int,

    @JsonIgnoreProperties(value = ["id"], allowSetters = true)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    val address: Address,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent_id", referencedColumnName = "id")
    var agent: Agent
)