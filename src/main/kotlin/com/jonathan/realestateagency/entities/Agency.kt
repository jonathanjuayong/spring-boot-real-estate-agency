package com.jonathan.realestateagency.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
@Table(name = "agencies")
data class Agency(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @Column(
        nullable = false,
        updatable = true,
        name = "name"
    )
    var name: String = "",

    @JsonIgnoreProperties(value = ["agency"], allowSetters = true)
    @OneToMany(mappedBy = "agency", fetch = FetchType.LAZY)
    var agents: List<Agent> = listOf()
)