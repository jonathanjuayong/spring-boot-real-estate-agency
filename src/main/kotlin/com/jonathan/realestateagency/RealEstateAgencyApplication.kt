package com.jonathan.realestateagency

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RealEstateAgencyApplication

fun main(args: Array<String>) {
	runApplication<RealEstateAgencyApplication>(*args)
}
