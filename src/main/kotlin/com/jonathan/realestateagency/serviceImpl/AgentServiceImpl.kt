package com.jonathan.realestateagency.serviceImpl

import com.jonathan.realestateagency.entities.Agent
import com.jonathan.realestateagency.repositories.AgentRepository
import com.jonathan.realestateagency.services.AgentService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class AgentServiceImpl(val agentRepository: AgentRepository) : AgentService {
    override fun getAllAgents(): List<Agent> =
        agentRepository.findAll().toList()

    override fun getAgentById(id: Long): Agent =
        agentRepository
            .findById(id)
            .orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "agent with id $id cannot be found")
            }


    override fun createAgent(agent: Agent): Agent {
        TODO("Not yet implemented")
    }

    override fun updateAgent(id: Long, agent: Agent): Agent {
        TODO("Not yet implemented")
    }

    override fun deleteAgent(id: Long) {
        TODO("Not yet implemented")
    }
}