package com.jonathan.realestateagency.serviceImpl

import com.jonathan.realestateagency.entities.Agency
import com.jonathan.realestateagency.repositories.AgencyRepository
import com.jonathan.realestateagency.services.AgencyService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class AgencyServiceImpl (val agencyRepository: AgencyRepository) : AgencyService {
    override fun getAllAgencies(): List<Agency> =
        agencyRepository.findAll()

    override fun getAgencyById(id: Long): Agency =
        agencyRepository
            .findById(id)
            .orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "agency with id $id is not found")
            }

    override fun createAgency(agency: Agency): Agency {
        TODO("Not yet implemented")
    }

    override fun updateAgency(id: Long, agency: Agency): Agency {
        TODO("Not yet implemented")
    }

    override fun deleteAgency(id: Long) {
        TODO("Not yet implemented")
    }
}