package com.jonathan.realestateagency.handlers

import com.jonathan.realestateagency.services.AgencyService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/")
class AgencyHandler (val agencyService: AgencyService) {

    @GetMapping("agencies")
    fun getAllAgencies() =
        agencyService.getAllAgencies()

    @GetMapping("agency/{id}")
    fun getAgencyById(@PathVariable("id") id: Long) =
        agencyService.getAgencyById(id)
}