package com.jonathan.realestateagency.handlers

import com.jonathan.realestateagency.entities.Agent
import com.jonathan.realestateagency.services.AgentService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/")
class AgentHandler (val agentService: AgentService) {

    @GetMapping("agents")
    fun getAllAgents() =
        agentService.getAllAgents()

    @GetMapping("agent/{id}")
    fun getAgentById(@PathVariable("id") id: Long): Agent =
        agentService.getAgentById(id)
}